# README #
Steps needed to get application up and running

### What is this repository for? ###

* Monad Assignment : Clinic Management System
* 					 Description: Admin creates a prescription for a patient, Doctor and Pharmacist can ask for permission to view prescription. Patient can give permission to view their prescription.
* Version 1.0

### How do I get set up? ###

* Clone the repository, Run migration and seeder.
* Checkout to git branch prod
* Create .env file from copying .env.example, generate key and add database username, password and database name.
* Install all dependcies to run Laravel 5.5 refer(https://laravel.com/docs/5.5/installation).
* Run (Composer Install) for install all Packages.
* Create a database and add the name in .env file.
* Create a virtual host and point it into /public folder under the project.
* Use Admin login : admin@monad.com, Use registeration form to create other users.

### Who do I talk to? ###

* Repo owner : Ashis Gupta
* Contact Email : agupta.rkl@gmail.com


